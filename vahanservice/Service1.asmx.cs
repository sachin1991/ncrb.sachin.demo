﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;

namespace vahanservice
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
        public byte[] VehicleSearch(string RegistrationNo, string Engineno, string Chasisno, string username)
        {
           
            DataSet ds = new DataSet();
            DataSet drt1;
            //Create a connection string.
            bool illegal = checkillegal(RegistrationNo, Chasisno, Engineno, username);
            if (illegal == false)
            {
                using (SqlConnection conn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString()))
                {
                    string output = string.Empty;
                    byte[] encrypted = null;
                    SqlCommand cmdpwd = new SqlCommand("SELECT [vcUserName],[vcPassword] FROM [TblMst_LoginDetails] where vcUserName=@username", conn);
                    cmdpwd.Parameters.AddWithValue("@username", username);
                    SqlDataAdapter adr = new SqlDataAdapter(cmdpwd);
                    DataTable dt = new DataTable();
                    adr.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            //Write path of stored procedure you created in SQL Server   
                            SqlCommand cmd = new SqlCommand("usp_getVehicleStatusWebService", conn);
                            //Write Command Type as here it is in form of stored procedure 
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@vcRegistration", RegistrationNo);
                            cmd.Parameters.AddWithValue("@vcEngineno", Engineno);
                            cmd.Parameters.AddWithValue("@vcChasisno", Chasisno);
                            cmd.Parameters.AddWithValue("@username", username);
                            SqlDataAdapter da = new SqlDataAdapter();
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                                //string result = DecryptStringFromBytes(encrypted, "71D932BCDBDC30120275EC8C393BD42A");

                            }
                            else
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    return encrypted;
                }
            }
            return encrptdata("Illegal Character Found.");            
        }
        [WebMethod]
        public byte[] VehicleSearchByRegistrationNo(string RegistrationNo, string username)
        {

            DataSet ds = new DataSet();
            DataSet drt1;
            //Create a connection string.
            bool illegal = checkillegalRegistrationNo(RegistrationNo, username);
            if (illegal == false)
            {
                using (SqlConnection conn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString()))
                {
                    string output = string.Empty;
                    byte[] encrypted = null;
                    SqlCommand cmdpwd = new SqlCommand("SELECT [vcUserName],[vcPassword] FROM [TblMst_LoginDetails] where vcUserName=@username", conn);
                    cmdpwd.Parameters.AddWithValue("@username", username);
                    SqlDataAdapter adr = new SqlDataAdapter(cmdpwd);
                    DataTable dt = new DataTable();
                    adr.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            //Write path of stored procedure you created in SQL Server   
                            SqlCommand cmd = new SqlCommand("usp_getVehicleStatusWebServiceRegistration", conn);
                            //Write Command Type as here it is in form of stored procedure 
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@vcRegistration", RegistrationNo);
                            cmd.Parameters.AddWithValue("@username", username);
                            SqlDataAdapter da = new SqlDataAdapter();
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                                //string result = DecryptStringFromBytes(encrypted, "71D932BCDBDC30120275EC8C393BD42A");

                            }
                            else
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    return encrypted;
                }
            }
            return encrptdata("Illegal Character Found.");
        }
        [WebMethod]
        public byte[] VehicleSearchByEngineNo(string Engineno, string username)
        {

            DataSet ds = new DataSet();
            DataSet drt1;
            //Create a connection string.
            bool illegal = checkillegalEngineNo(Engineno, username);
            if (illegal == false)
            {
                using (SqlConnection conn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString()))
                {
                    string output = string.Empty;
                    byte[] encrypted = null;
                    SqlCommand cmdpwd = new SqlCommand("SELECT [vcUserName],[vcPassword] FROM [TblMst_LoginDetails] where vcUserName=@username", conn);
                    cmdpwd.Parameters.AddWithValue("@username", username);
                    SqlDataAdapter adr = new SqlDataAdapter(cmdpwd);
                    DataTable dt = new DataTable();
                    adr.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            //Write path of stored procedure you created in SQL Server   
                            SqlCommand cmd = new SqlCommand("usp_getVehicleStatusWebServiceEngineNo", conn);
                            //Write Command Type as here it is in form of stored procedure 
                            cmd.CommandType = CommandType.StoredProcedure;                            
                            cmd.Parameters.AddWithValue("@vcEngineno", Engineno);
                            cmd.Parameters.AddWithValue("@username", username);
                            SqlDataAdapter da = new SqlDataAdapter();
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                                //string result = DecryptStringFromBytes(encrypted, "71D932BCDBDC30120275EC8C393BD42A");

                            }
                            else
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    return encrypted;
                }
            }
            return encrptdata("Illegal Character Found.");
        }
        [WebMethod]
        public byte[] VehicleSearchByChasisNo(string Chasisno, string username)
        {

            DataSet ds = new DataSet();
            DataSet drt1;
            //Create a connection string.
            bool illegal = checkillegalChasisNo(Chasisno,username);
            if (illegal == false)
            {
                using (SqlConnection conn = new SqlConnection(System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Conn"].ToString()))
                {
                    string output = string.Empty;
                    byte[] encrypted = null;
                    SqlCommand cmdpwd = new SqlCommand("SELECT [vcUserName],[vcPassword] FROM [TblMst_LoginDetails] where vcUserName=@username", conn);
                    cmdpwd.Parameters.AddWithValue("@username", username);
                    SqlDataAdapter adr = new SqlDataAdapter(cmdpwd);
                    DataTable dt = new DataTable();
                    adr.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        try
                        {
                            //Write path of stored procedure you created in SQL Server   
                            SqlCommand cmd = new SqlCommand("usp_getVehicleStatusWebServiceChasisNo", conn);
                            //Write Command Type as here it is in form of stored procedure 
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@vcChasisno", Chasisno);
                            cmd.Parameters.AddWithValue("@username", username);
                            SqlDataAdapter da = new SqlDataAdapter();
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                                //string result = DecryptStringFromBytes(encrypted, "71D932BCDBDC30120275EC8C393BD42A");

                            }
                            else
                            {
                                output = DatasetToString(ds);
                                encrypted = encrptdata(output);
                            }

                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    return encrypted;
                }
            }
            return encrptdata("Illegal Character Found.");
        }

        public byte[] encrptdata(string original)
        {
            string output = string.Empty;
            byte[] encrypted = null;
            try
            {
                string pwd;
                using (RijndaelManaged myRijndael = new RijndaelManaged())
                {
                    myRijndael.GenerateKey();
                    myRijndael.GenerateIV();
                    // Encrypt the string to an array of bytes.
                    encrypted = EncryptStringToBytes(original, myRijndael.Key, myRijndael.IV);


                    //output = GetString(encrypted);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.Message);
            }
            return encrypted;
        }
        private String DatasetToString(DataSet ds)
        {
            Int32 i = 0;
            String str = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["Status"].ToString().Trim() == "Record Not available")
                {
                    str = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><VehicleDetails><status>" + ds.Tables[0].Rows[0]["Status"].ToString().Trim() + "</status></vehicleDetails>";
                }
                else
                {
                    str = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?><VehicleDetails><status>" + ds.Tables[0].Rows[0]["Status"].ToString().Trim() + "</status><regno>" + ds.Tables[0].Rows[0]["regnokey"].ToString().Trim() + "</regno><chasisno>" + ds.Tables[0].Rows[0]["chasisnokey"].ToString().Trim() + "</chasisno><enginno>" + ds.Tables[0].Rows[0]["enginenokey"].ToString().Trim() + "</enginno><firno>" + ds.Tables[0].Rows[0]["FIR"].ToString().Trim() + "</firno><firdate>" + ds.Tables[0].Rows[0]["FIRDATE"].ToString().Trim() + "</firdate><vehicletype>" + ds.Tables[0].Rows[0]["autotypedesc"].ToString().Trim() + "</vehicletype><make>" + ds.Tables[0].Rows[0]["automakedesc"].ToString().Trim() + "</make><ps>" + ds.Tables[0].Rows[0]["psname"].ToString().Trim() + "</ps><district>" + ds.Tables[0].Rows[0]["distname"].ToString().Trim() + "</district><state>" + ds.Tables[0].Rows[0]["stdesc"].ToString().Trim() + "</state></VehicleDetails>";
                }
            }
            return (str);
        }
        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {


            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                //rijAlg.Key = Key;
                //rijAlg.IV = IV;
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.KeySize = 128;
                rijAlg.BlockSize = 128;

                byte[] keyBytes = new byte[16];
                rijAlg.Key = keyBytes;
                rijAlg.IV = keyBytes;
                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }
        [WebMethod]
        public string DecryptStringFromBytes(byte[] cipherText, string Key)
        {
            string plaintext = null;
            string password = "71D932BCDBDC30120275EC8C393BD42A";
            // Check arguments.
            if (Key == password)
            {
                if (cipherText == null || cipherText.Length <= 0)
                    throw new ArgumentNullException("cipherText");


                // Declare the string used to hold
                // the decrypted text.


                // Create an RijndaelManaged object
                // with the specified key and IV.
                using (RijndaelManaged rijAlg = new RijndaelManaged())
                {
                    rijAlg.Mode = CipherMode.CBC;
                    rijAlg.Padding = PaddingMode.PKCS7;
                    rijAlg.KeySize = 128;
                    rijAlg.BlockSize = 128;

                    byte[] keyBytes = new byte[16];
                    rijAlg.Key = keyBytes;
                    rijAlg.IV = keyBytes;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                    // Create the streams used for decryption.
                    using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {

                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }

                }
            }
            else
            {
                plaintext = "";
            }
            return plaintext;
        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        private Boolean checkillegal(string regno, string chasis, string enginno, string username)
        {
            bool illegal;
            illegal = false;
            if (regno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (chasis.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (enginno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (username.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (illegal)
            {
                return true;
            }
            {
                return false;
            }
        }
        private Boolean checkillegalRegistrationNo(string regno, string username)
        {
            bool illegal;
            illegal = false;
            if (regno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }           
            if (username.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (illegal)
            {
                return true;
            }
            {
                return false;
            }
        }
        private Boolean checkillegalEngineNo(string enginno, string username)
        {
            bool illegal;
            illegal = false;            
            if (enginno.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (username.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (illegal)
            {
                return true;
            }
            {
                return false;
            }
        }
        private Boolean checkillegalChasisNo(string chasis, string username)
        {
            bool illegal;
            illegal = false;            
            if (chasis.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }            
            if (username.IndexOfAny(new char[] { '[', ';', '\\', '/', ':', '*', '?', '#', '"', '<', '>', '&', '%', '^', ']', '|', '\'' }) >= 0)
            {
                illegal = true;
            }
            if (illegal)
            {
                return true;
            }
            {
                return false;
            }
        }


    }

}